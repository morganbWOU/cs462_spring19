﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace SpecFlow_Selenium_DEMO
{
    class Selenium_Control
    {
        static void Main(string[] args)
        {

        }

        [SetUp]
        public void Initialize()
        {
            PropertiesCollection.driver = new ChromeDriver();

            //Navigate to google page
            PropertiesCollection.driver.Navigate().GoToUrl("http://executeautomation.com/demosite/Login.html");
        }

        [Test]
        public void ExecuteTest()
        {


            LoginPageObject pageLogin = new LoginPageObject();
            EAPageObject pageEA = pageLogin.Login("execute", "automation");

            pageEA.FillUserForm("M", "Trouble", "Becka");
            
            
            
            ////Title
            //SeleniumSetMethods.SelectDropDown("TitleId", "Mr.", PropertyType.Id);

            ////Initital
            //SeleniumSetMethods.EnterText("Initial", "executeautomation", PropertyType.Name);

            ////Click
            //SeleniumSetMethods.Click("Save", PropertyType.Name);

            //Console.WriteLine("The value of  my title is: " + SeleniumGetMethods.GetText("TitleId", PropertyType.Id));

            //Console.WriteLine("The value of  my Initial is: " + SeleniumGetMethods.GetText("Initial", PropertyType.Name));



        }


        [TearDown]
        public void CleanUp()
        {
            PropertiesCollection.driver.Quit();
            Console.WriteLine("Closed the browser");
        }
    }
}
