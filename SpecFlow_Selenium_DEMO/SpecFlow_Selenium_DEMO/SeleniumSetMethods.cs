﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;

namespace SpecFlow_Selenium_DEMO
{

    class SeleniumSetMethods
    {
        
        //Enter Text
        public static void EnterText(IWebElement element, string value)
        {
            element.SendKeys(value);
        }

        //Click into button, checkbox, option, etc.
        public static void Click(IWebElement element)
        {
            element.Click();
        }

        //Selectin a dropdown control
        public static void SelectDropDown(IWebElement element, string value)
        {
             
            new SelectElement(element).SelectByText(value);
            
        }
    }
}
